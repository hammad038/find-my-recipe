import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import {createAppContainer} from 'react-navigation';
import {Platform} from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import Colors from '../constants/Colors';

// screens imports.
import Categories from '../screens/Categories';
import CategoryMeals from '../screens/CategoryMeals';
import MealDetail from '../screens/MealDetail';
import Favorites from '../screens/Favorites';
import Filters from '../screens/Filters';

// default nav options for header user in all and fav navigator
const defaultStackNavOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === 'android' ? Colors.primaryColor : '',
  },
  headerTitleStyles: {
    fontFamily: 'open-sans',
  },
  headerTintColor: Platform.OS === 'android' ? 'white': Colors.primaryColor,
};

// all meals navigator
const MealsNavigator = createStackNavigator({
  Categories: {
    screen: Categories,
  },
  CategoryMeals: {
    screen: CategoryMeals,
  },
  MealDetail: {
    screen: MealDetail,
  },
}, {
  // initialRouteName: 'Categories',
  defaultNavigationOptions: defaultStackNavOptions,
});

// fav meals navigator
const FavoritesNavigator = createStackNavigator({
  Favorites: {
    screen: Favorites,
  },
  MealDetail: {
    screen: MealDetail,
  },
}, {
  // initialRouteName: 'Categories',
  defaultNavigationOptions: defaultStackNavOptions,
});

// tabs configuration
const tabsScreenConfig = {
  Meals: {
    screen: MealsNavigator,
    navigationOptions: {
      tabBarLabel: 'All',
      tabBarIcon: (tabInfo) => {
        return <Ionicons name='ios-restaurant' size={25} color={tabInfo.tintColor}/>
      },
      tabBarColor: Colors.primaryColor
    },
  },
  Favorites: {
    screen: FavoritesNavigator,
    navigationOptions: {
      tabBarLabel: '',
      tabBarIcon: (tabInfo) => {
        return <Ionicons name='ios-star' size={25} color={tabInfo.tintColor}/>
      },
      tabBarColor: Colors.accentColor
    },
  },
};

const MealsFavTabNavigator = Platform.OS === 'android'
  ? createMaterialBottomTabNavigator(tabsScreenConfig, {
    activeTintColor: Colors.accentColor,
    shifting: true
  })
  : createBottomTabNavigator(tabsScreenConfig, {
  tabBarOptions: {
    activeTintColor: Colors.accentColor,
    activeBackgroundColor: Colors.primaryColor
  }
});

const FiltersNavigator = createStackNavigator({
  Filters: Filters
}, {
  // initialRouteName: 'Categories',
  defaultNavigationOptions: defaultStackNavOptions,
});

// sidedrawer navigator
const MainNavigator = createDrawerNavigator({
  Favorite: {
    screen: MealsFavTabNavigator,
    navigationOptions: {
      drawerLabel: 'Meals'
    },
  },
  Filters: {
    screen: FiltersNavigator,
  },
}, {
  contentOptions: {
    activeTintColor: Colors.accentColor,
    labelStyle: {
      textAlign: 'right',
      fontFamily: 'open-sans-bold'
    },
  }
});

export default createAppContainer(MainNavigator);
