import {MEALS} from '../../data/dummy-data';
import {SET_FILTERS, TOGGLE_FAVORITE} from '../actions/meals';

const initialState = {
  meals: MEALS,
  filteredMeals:  MEALS,
  favorites: [],
};

const mealsReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_FAVORITE: {
      const existingIndex = state.favorites.findIndex(meal => meal.id === action.mealId);
      if(existingIndex >= 0) {
        // remove if the meal already exist in favs
        const updatedFavMeals = [...state.favorites];
        updatedFavMeals.splice(existingIndex, 1);
        return {...state, favorites: updatedFavMeals}
      } else {
        // add meal in favs
        const meal = state.meals.find(meal => meal.id === action.mealId);
        return {...state, favorites: state.favorites.concat(meal)};
      }
    }

    case SET_FILTERS: {
      const appliedFilters = action.filters;
      const updatedFilteredMeals = state.meals.filter(meal => {
        if(appliedFilters.glutenFree && !meal.isGlutenFree) {
          return false;
        }
        if(appliedFilters.lactoseFree && !meal.isLactoseFree) {
          return false;
        }
        if(appliedFilters.vegan && !meal.isVegan) {
          return false;
        }
        if(appliedFilters.vegetarian && !meal.isVegetarian) {
          return false;
        }
        return true;
      })
      return {
        ...state,
        filteredMeals: updatedFilteredMeals
      };
    }

    default:
      return state;
  }
}

export default mealsReducer;
