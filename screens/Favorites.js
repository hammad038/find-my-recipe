import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import MealsList from '../components/MealsList';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import HeaderButton from '../components/HeaderButton';
import {useSelector} from 'react-redux';
import DefaultText from '../components/DefaultText';

const Favorites = (props) => {
  const favoriteMeals = useSelector((state) => state.meals.favorites);

  if(favoriteMeals.length === 0 || !favoriteMeals) {
    return (<View style={styles.content}><DefaultText>No favorite Meals selected.</DefaultText></View>);
  } else {
    return (<MealsList listData={favoriteMeals} navigation={props.navigation}/>);
  }
};

Favorites.navigationOptions = (navData) => {
  return {
    headerTitle: 'Your Favorite Meals',
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title='Menu'
          iconName='ios-menu'
          onPress={() => {navData.navigation.toggleDrawer()}}
        />
      </HeaderButtons>
    ),
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10
  },
});

export default Favorites;
