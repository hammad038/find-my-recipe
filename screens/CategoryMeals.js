import React, {useState} from 'react';
import {useSelector} from 'react-redux';

import {CATEGORIES} from '../data/dummy-data';
import MealsList from '../components/MealsList';
import {StyleSheet, View} from 'react-native';
import DefaultText from '../components/DefaultText';

const CategoryMeals = (props) => {
  const categoryId = props.navigation.getParam('categoryId');
  const availableMeals = useSelector((state) => state.meals.filteredMeals);
  const displayedMeals = availableMeals.filter(meal => meal.categoryIds.indexOf(categoryId) >= 0);

  if(displayedMeals.length === 0) {
    return (<View style={styles.content}><DefaultText>No Meal found, Please check your filters.</DefaultText></View>);
  } else {
    return (<MealsList listData={displayedMeals} navigation={props.navigation}/>);
  }
};

// set navigation title
CategoryMeals.navigationOptions = (navigationData) => {
  const categoryId = navigationData.navigation.getParam('categoryId');
  const selectedCategory = CATEGORIES.find(category => category.id === categoryId);

  return {
    headerTitle: selectedCategory.title,
  };
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10
  },
});

export default CategoryMeals;
