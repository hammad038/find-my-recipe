import React, {useState} from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import MealItem from './MealItem';
import {useSelector} from 'react-redux';

const MealsList = (props) => {
  const favoriteMeals = useSelector(state => state.meals.favorites);

  const renderMealItem = itemData => {
    const isFav = favoriteMeals.some(meal => meal.id === itemData.item.id);

    return <MealItem
      title={itemData.item.title}
      image={itemData.item.imageUrl}
      duration={itemData.item.duration}
      complexity={itemData.item.complexity}
      affordability={itemData.item.affordability}
      onSelectMeal={() =>
        props.navigation.navigate({
          routeName: 'MealDetail',
          params: {
            mealId: itemData.item.id,
            mealTitle: itemData.item.title,
            isFav: isFav,
          },
        })
      }
    />;
  }

  return (
    <View style={styles.list}>
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={props.listData}
        renderItem={renderMealItem}
        style={{width: '100%'}}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  list: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10
  },
});

export default MealsList;
